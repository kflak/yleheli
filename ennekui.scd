(
~ennekuiFx = FxChain.new(
    level: 0.dbamp,
    out: ~mainBus,
    fadeInTime: 1,
    fadeOutTime: 1,
);

~ennekuiFx.addPar(
    \comb, [\mix, 0.2, \delay, 0.2, \decay, 1],
    \comb, [\mix, 0.2, \delay, 0.3, \decay, 1],
    \comb, [\mix, 0.2, \delay, 0.5, \decay, 1],
);

~ennekuiFx.add(\flanger,[
    \feedback, 0.12,
    \depth, 0.09,
    \rate, 0.03,
    \decay, 0.01,
    \mix, 0.3,
]);

~ennekuiFx.add(\jpverb,[
    \revtime, 2,
    \mix, 0.2,
]);

~ennekuiFx.add(\eq,[
    \hishelffreq, 400,
    \hishelfdb, -6,
    \mix, 1.0,
]);

// ~ennekuiFx.play;

~ennekui = ~mb.collect{|id, idx|
    MBDeltaTrig.new(
        speedlim: 0.5, 
        threshold: 0.0125, 
        minibeeID: id,
        minAmp: -0,
        maxAmp: 24,
        function: {|dt, minAmp, maxAmp|
            var duration = dt.linlin(0.0, 1.0, 1, 9);
            var buf = ~buf[\ennekui][0];
            var start = rrand(0, buf.numFrames - 1);
            var end = rrand(0, buf.numFrames - 1);
            Pfindur(
                duration,
                Pbind(
                    \instrument, \playbuf,
                    \buf, buf,
                    \bufframes, Pfunc{|ev| ev.buf.numFrames },
                    \buflength, Pfunc{|ev| ev.buf.duration },
                    \stretchfactor, Pwhite(0.5, 1.0),
                    \startPos, Pseg([start, end], Pkey(\buflength) * rrand(0.2, 1)),
                    \pan, Pseg(Pwhite(-1.0, 1.0), Pkey(\buflength) * Pkey(\stretchfactor)),
                    \dur, Pwhite(0.05, 1.9),
                    \attack, Pkey(\dur) * Pwhite(0.5, 1.0),
                    \release, Pkey(\dur) * Pwhite(0.5, 1.0),
                    \legato, Pfunc{ ~mbData[id].delta.linlin(0.0, 1.0, 0.2, 4.0).clip(0.0, 1.5) },
                    \envelope, Pseg([-70, 0, 0, -70], [0.1, 0.5, 0.4] * duration),
                    \db, Pfunc{|ev|
                        var env = ev.envelope;
                        ~mbData[id].delta.linlin(0.0, 1.0, minAmp, maxAmp).clip(-70, 6) + env;
                    },
                    \group, ~ennekuiFx.group,
                    \out, ~ennekuiFx.in,
                )
            ).play;
        }
    );
}
)

~ennekuiFx.play; ~ennekui.do(_.play);
~ennekuiFx.free; ~ennekui.do(_.free);
