# Üle Heli

SuperCollider source code for [RoosnaFlak's](https://roosnaflak.com) upcoming concert/dance performance at the Üle Heli festival in Estonia.

## Dependencies
To run the examples you need to install a few dependencies. Unfortunately not very well documented yet, and solidly alpha quality, untested on most other platforms than Arch Linux (except SC3-plugins and MKPlugins)

- [SC3-plugins](https://github.com/supercollider/sc3-plugins)
- [MiniBeeUtils](https://gitlab.com/kflak/minibeeutils)
- [KFUtils](https://gitlab.com/kflak/kfutils)
- [MKPlugins](https://github.com/madskjeldgaard/mkplugins)

## Usage
Run `init.scd` in your development environment of choice to set up the initial variables and synthdefs.
